using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    int goalCounter = 0;
    [SerializeField] private UnityEngine.AI.NavMeshAgent agent;
    GameObject[] points;
    void Start()
    {
        points = GameObject.FindGameObjectsWithTag("Points");
        agent.SetDestination(points[0].transform.position);
    }

    // Update is called once per frame
    void Update()
    {
        Walk();
    }
    void Walk()
    {
        if (goalCounter >= 3)
        {
            goalCounter = 0;
        }
        if (agent.remainingDistance <= 0.05f)
        {
            agent.SetDestination(points[goalCounter++].transform.position);
        }

    }
}
